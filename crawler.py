import urllib2

page = "www.manipal.edu"

opener = urllib2.build_opener()
opener.addheaders = [{"User-agent","Chrome"}]

def get_page(url):
	print "getting ",url
	ourPage = opener.open(url)
	s = ourPage.read()
	print "Web Page Fetched"
	return s

def get_next_target(page):
    start_link = page.find('<a href=')
    if start_link == -1: 
        return None, 0
    start_quote = page.find('"', start_link)
    end_quote = page.find('"', start_quote + 1)
    url = page[start_quote + 1:end_quote]
    return url, end_quote

def union(p,q):
    for e in q:
        if e not in p:
            p.append(e)

def get_all_links(page):
    links = []
    while True:
        url,endpos = get_next_target(page)
        if url:
			if url.find("support")==1:
				continue
			if url[0:4] == "http":
				links.append(url)
			page = page[endpos:]
        else:
            break
    return links

def crawl_web(seed,max_pages):
    tocrawl = set([seed])
    crawled = set([])
    index = {}
    while tocrawl:
        page = tocrawl.pop()
        if page not in crawled:
            content = get_page(page)
            add_page_to_index(index,page,content)
            tocrawl.update(get_all_links(content))
            crawled.update(page)
        if len(crawled)==max_pages:
            break
    return index
def add_page_to_index(index,page,content):
    words = remove_tags(content)
    for i in words:
        add_to_index(index,i,page)

def remove_tags(content):
    start = content.find("<")
    end = 0
    while start!=-1:
        end = content.find(">",start)
        content = content[:start]+" "+content[end+1:]
        start = content.find("<")
    return content.split()
#--------------------------------------------- Indexing part----------------------------------------------------#
def add_to_index(index,keyword,url):
    if keyword not in index:
        index[keyword] = [url]
    else:
        index[keyword].append(url)

def lookup(index,keyword):
    if keyword in index:
        return index[keyword]
    else:
        return None
def hash_string(keyword,no_of_buckets):
    value = 0;
    for i in keyword:
        value = value + ord(i)
    return value%no_of_buckets
# Creating an empty hash-table
def make_hashtable(n_buckets):
    index = []
    for i in range(n_buckets):
        index.append([])
    return index
# get the bucket
def hashtable_get_bucket(table,word):
    no_of_buckets = len(table)
    return table[hash_string(word,no_of_buckets)]
# this redundantly add's the key's
def hashtable_add(table,word,value):
    buck = hashtable_get_bucket(table,word)
    buck.append([word,value])
# searching using hashing
def hashtable_lookup(htable,key):
    val = hash_string(key,len(htable))
    list = htable[val]
    for i in list:
        if i[0]==key:
            return i[1]
    return None
#update the value in the table
def hashtable_update(htable,key,value):
    bucket = htable[hash_string(key,len(htable))]
    for i in bucket:
        if i[0] == key:
            i[1] = value
            return htable
    bucket.append([key,value])
    return htable

links =  crawl_web("http://www.facebook.com",5) 
